Kullanıcının mevcut konumundaki anlık hava durumunu yerel veri tabanına kaydeder.

Kullanılan araç ve kütüphaneler;  
-**OpenWeatherMap -> CurrentWeatherData API**  
-**URLSession**  
-**CoreLocation**  
-**RealmSwift**  
-**Table View**  
-**Alert View**  

![weather-home.png](https://bitbucket.org/asengur/weather-logger/raw/master/WeatherLogger/Screenshots/weather-home.png)
![weather-detail.png](https://bitbucket.org/asengur/weather-logger/raw/master/WeatherLogger/Screenshots/weather-detail.png)

