//
//  DatabaseManager.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import RealmSwift


class DatabaseManager {
    
    static let shared = DatabaseManager()
    
    
    func saveCurrentWeatherInfos(location: String, country: String, main: String, weatherDescription: String, temperature: Int, iconData: Data, date: String, minTemp: Int, maxTemp: Int, feelsLike: Int, humidity: Int, completion: (Bool) -> Void) {
        
        let realm = try! Realm()
        
        let currentWeather = CurrentWeather()
        currentWeather.locationName = location
        currentWeather.country = country
        currentWeather.main = main
        currentWeather.weatherDescription = weatherDescription
        currentWeather.temp = temperature
        currentWeather.weatherIconData = iconData
        currentWeather.date = date
        currentWeather.tempMin = minTemp
        currentWeather.tempMax = maxTemp
        currentWeather.feelsLike = feelsLike
        currentWeather.humidity = humidity
        
        
        do {
            realm.beginWrite()
            realm.add(currentWeather) // add current weather to database
            try realm.commitWrite()
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    
}

