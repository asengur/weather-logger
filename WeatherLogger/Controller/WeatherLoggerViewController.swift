//
//  WeatherLoggerViewController.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import CoreLocation
import RealmSwift



class WeatherLoggerViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    
    
    //MARK: - Properties
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var currentWeather: CurrentLocalWeather? // to store current weather infos from API
    var weatherIconData: Data? // to store icon data of current weather
    var weathers: Results<CurrentWeather>? // to store weather info from realm database
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        saveButton.layer.cornerRadius = 5
        setupLocation()
        getWeatherDataFromDatabase()
        setupTableView()
    }
    
    
    
    // setup table view
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        let cellNib = UINib(nibName: "WeatherLoggerTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "WeatherLoggerTableViewCell")
    }
    
    
    
    // get weather informations from realm database
    func getWeatherDataFromDatabase() {
        let realm = try! Realm()
        weathers = realm.objects(CurrentWeather.self)
    }
    

    
    
    //MARK: - get current weather informations from api
    func getCurrentWeather(latitude: Double, longitude: Double) {
        
        APIRequest.shared.getCurrentWeather(lat: latitude, lon: longitude) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let currentLocalWeather):
                self?.currentWeather = currentLocalWeather
                self?.getWeatherIcon(icon: currentLocalWeather.weather.first!.icon) /// call function with icon string
            }
        }
    }
    
    
    
    //MARK: - gets icon data from api
    func getWeatherIcon(icon: String) {
        APIRequest.shared.getWeatherIcon(icon: icon) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let iconData):
                self?.weatherIconData = iconData
            }
        }
    }
    
    
    
    
    
    //MARK: - save weather informations to realm database
    @IBAction func saveCurrentWeather(_ sender: UIButton) {
        
        /// date formatter
        let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = "d MMM yyyy HH:mm"
        
        guard let currentWeather = self.currentWeather,
            let main = currentWeather.weather.first?.main,
            let iconData = weatherIconData
            else { return }

        
        DatabaseManager.shared.saveCurrentWeatherInfos(location: currentWeather.name,
                                                       country: currentWeather.sys.country,
                                                       main: main,
                                                       weatherDescription: currentWeather.weather.first!.description,
                                                       temperature: (Int(round(currentWeather.main.temp))),
                                                       iconData: iconData,
                                                       date: formatter.string(from: date),
                                                       minTemp: (Int(round(currentWeather.main.tempMin))),
                                                       maxTemp: (Int(round(currentWeather.main.tempMax))),
                                                       feelsLike: (Int(round(currentWeather.main.feelsLike))),
                                                       humidity: (currentWeather.main.humidity)) { success in
                                                        if success {
                                                            DispatchQueue.main.async {
                                                                self.tableView.reloadData()
                                                            }
                                                        }
        }
    }
}



//MARK: - Core Location Functions
extension WeatherLoggerViewController: CLLocationManagerDelegate {
    
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    
    
    // this function will be called whenever location information is updated
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            
            guard let latitude = currentLocation?.coordinate.latitude,
                let longitude = currentLocation?.coordinate.longitude else {
                    return
            }
            
            getCurrentWeather(latitude: latitude, longitude: longitude) /// get current weather infos from api by coordinates
        }
    }
    
}







//MARK: - Table View Functions
extension WeatherLoggerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let weathers = weathers {
            return weathers.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherLoggerTableViewCell", for: indexPath) as! WeatherLoggerTableViewCell
        cell.delegate = self
        cell.index = indexPath
        if let weathers = weathers {
            cell.configure(weather: weathers[indexPath.row], iconData: weathers[indexPath.row].weatherIconData!, date: weathers[indexPath.row].date)
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
    // simple animation for table view cells
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0

        UIView.animate(
            withDuration: 0.5,
            delay: 0.05 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
    
    
    
    // swipe to delete from table view and realm database
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let weather = weathers?[indexPath.row] {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(weather)
                }
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
        }
    }
}



//MARK: - Show Details
extension WeatherLoggerViewController: WeatherTableViewCellDelegate {
    func didTapShowDetails(index: Int) {
        if let detailsVC = self.storyboard?.instantiateViewController(identifier: "WeatherDetailsViewController") as? WeatherDetailsViewController {
            guard let weathers = weathers else { return }
            detailsVC.weather = weathers[index]
            detailsVC.modalTransitionStyle = .flipHorizontal
            self.navigationController?.pushToDetailsViewController(controller: detailsVC) // transition
        }
    }
}





