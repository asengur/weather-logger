//
//  WeatherDetailsViewController.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class WeatherDetailsViewController: UIViewController {

    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempMin: UILabel!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var humidity: UILabel!
    
    
    var weather: CurrentWeather?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    
    public func setupUI() {
        
        guard let weather = weather else { return }
        print("weather: \(weather)")
        
        DispatchQueue.main.async {
            self.tempLabel.text = "\(weather.temp)°C"
            self.weatherIcon.image = UIImage(data: weather.weatherIconData!)
            self.mainLabel.text = weather.main
            self.locationLabel.text = weather.locationName
            self.countryLabel.text = weather.country
            self.dateLabel.text = weather.date
            self.descriptionLabel.text = weather.weatherDescription
            self.tempMin.text = "\(weather.tempMin)°C"
            self.tempMax.text = "\(weather.tempMax)°C"
            self.feelsLike.text = "\(weather.feelsLike)°C"
            self.humidity.text = "\(weather.humidity)"
        }
    }

}
