//
//  WeatherLoggerTableViewCell.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit


//MARK: - Delegate Protocol Pattern

protocol WeatherTableViewCellDelegate {
    func didTapShowDetails(index: Int)
}



class WeatherLoggerTableViewCell: UITableViewCell {

    

    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    
    
    
    var delegate: WeatherTableViewCellDelegate?
    var index: IndexPath?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    public func configure(weather: CurrentWeather, iconData: Data, date: String) {
        self.tempLabel.text = "\(weather.temp)°"
        self.locationNameLabel.text = weather.locationName
        self.countryLabel.text = weather.country
        self.iconImage.image = UIImage(data: iconData)
        self.dateLabel.text = date
    }
    
    
    @IBAction func showDetails(_ sender: UIButton) {
        delegate?.didTapShowDetails(index: (index!.row))
    }
}
