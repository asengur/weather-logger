//
//  CurrentWeather.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import RealmSwift


class CurrentWeather: Object {
    
    @objc dynamic var temp: Int = 0
    @objc dynamic var tempMin: Int = 0
    @objc dynamic var tempMax: Int = 0
    @objc dynamic var feelsLike: Int = 0
    @objc dynamic var humidity: Int = 0
    @objc dynamic var main: String = ""
    @objc dynamic var weatherDescription: String = ""
    @objc dynamic var locationName: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var weatherIconData: Data? = nil
    @objc dynamic var date: String = ""
    @objc dynamic var windSpeed: Double = 0.0
    
}
