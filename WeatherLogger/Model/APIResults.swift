//
//  APIResults.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



struct CurrentLocalWeather: Codable {
    let weather: [Weather]
    let main: Main
    let name: String
    let sys: Sys
    let wind: Wind
}


struct Weather: Codable {
    let main: String
    let description: String
    let icon: String
}


struct Main: Codable {
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let feelsLike: Double
    let humidity: Int
}


struct Sys: Codable {
    let country: String
}


struct Wind: Codable {
    let speed: Double
}
