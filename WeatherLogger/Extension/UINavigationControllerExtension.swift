//
//  UINavigationControllerExtension.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 20.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import UIKit


extension UINavigationController {
    
    func pushToDetailsViewController(controller: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = .push
        transition.subtype = .fromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        pushViewController(controller, animated: false)
    }
    
}
