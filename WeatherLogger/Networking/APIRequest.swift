//
//  APIRequest.swift
//  WeatherLogger
//
//  Created by Ali Şengür on 17.09.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation


//MARK: - error enum
enum APIError: Error {
    case noDataAvailable
    case canNotProcessData
}



//MARK: - api requests
final class APIRequest {

    static let shared = APIRequest()
    let baseURLString = "https://api.openweathermap.org/data/2.5/weather?"
    let apiKey = "17c5219f6f03a2ceefba7f67107edff5"



    //MARK: - Get current weather informations from api
    func getCurrentWeather(lat: Double, lon: Double, completion: @escaping(Result<CurrentLocalWeather, APIError>) -> Void) {
        let resourceUrlString = "\(baseURLString)lat=\(lat)&lon=\(lon)&appid=\(apiKey)&units=metric"
        guard let resourceUrl = URL(string: resourceUrlString) else {
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: resourceUrl) { data, _, _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase /// convert from snake case to camel case
                let result = try decoder.decode(CurrentLocalWeather.self, from: jsonData)
                completion(.success(result))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
    
    
    
    //MARK: - Get weather icon data from api
    func getWeatherIcon(icon: String, completion: @escaping(Result<Data, APIError>) -> Void) {
        let resourceUrlString = "http://openweathermap.org/img/w/\(icon).png"
        guard let resourceUrl = URL(string: resourceUrlString) else {
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: resourceUrl) { iconData, _, _ in
            if let data = iconData {
                completion(.success(data))
            }
            completion(.failure(.noDataAvailable))
        }
        dataTask.resume()
    }

}
